﻿using System;
namespace oops
{
    class myBase
    {
        
        public virtual void VirtualMethod()
        {
            Console.WriteLine("base class");
        }
    }

    class myDerived : myBase
    {
        
        public override void VirtualMethod()
        {
            Console.WriteLine("Derive class");
        }
    }
    class virtualClass
    {
        static void Main(string[] args)
        {
            
            new myDerived().VirtualMethod();
            Console.ReadKey();
        }
    }
}