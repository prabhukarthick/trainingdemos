﻿class Program
{
    public void square(int nmbr)
    {
        int nmbr = nmbr * nmbr;
         
        Console.WriteLine("Square of the given number is  " + nmbr);
    }

    public static void Main(string[] args)
    {
        int nmbr = 2; 
        Program pr = new Program();   

        pr.square(ref nmbr); 
        Console.WriteLine("The given number is  " + nmbr);     

    }
}
}