﻿using System;

namespace abstractclass
{
    class Program
    {
        abstract class shape
        {
            public abstract void area();
        }

        class circle : shape
        {
            public override void area()
            {
                Console.WriteLine("i am abstract class");
            }
                
            static void Main(string[] arg)
            {
                circle b = new circle();
                b.area();
                Console.ReadKey();
            }
        }
      
    }
   
}
